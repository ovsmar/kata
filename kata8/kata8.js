var panier = {
    products: [],
    total: 0,
    addProduct: function(product){
        this.products.push(product);
        this.calculateTotal();
      },
      calculateTotal: function(){
        var total = 0;
        for(var i = 0; i < this.products.length; i++){
          total += this.products[i].price;
        }
        this.total = Math.round(total*100) / 100;
      }
    };